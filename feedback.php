<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->ShowHead();
$APPLICATION->ShowPanel();
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap_v4/bootstrap.css");
$APPLICATION->SetTitle("Отзывы");

/*Подключаемся к бд*/ 
$connection = \Bitrix\Main\Application::getConnection();

/*Берем отзывы для отображения на странице name,text*/
$sqlSelectFeedbackText = ("SELECT text FROM `feedback_text`");
$sqlSelectFeedback = ("SELECT name FROM `feedback`");
$resultFeedbackText = $connection->query($sqlSelectFeedbackText);
$resultFeedback = $connection->query($sqlSelectFeedback);

/*Проверяем входящие данные с формы*/
if(empty($_POST['name']) && empty($_POST['email']) && empty($_POST['text'])){
    echo "Ошибка, не возможно отправить отзыв. Причина: <b>Заполните все поля. </b>";      
}else{
    $connection->query("INSERT INTO feedback(name,email) VALUE('$name','$email')");
    $connection->query("INSERT INTO feedback_text(text) VALUE('$text')");
    header('Location: http://localhost/feedback.php');
}
?>

<form action="feedback.php" method="POST">
<div class="form-group">
    <label for="exampleFormControlInput1">Имя</label>
        <input type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="Имя" required > 

    <label for="exampleFormControlInput1">Почтовый ящик</label>
        <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" required>
</div>
<div class="form-group">
    <label for="exampleFormControlTextarea1">Напишите отзыв</label>
        <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="3" required ></textarea>
</div>
<button type="submit" class="btn btn-success">Отправить</button>
</form>
Отзывы:
<? 

while($rowFeedback = $resultFeedback->fetch() and $rowFeedbackText = $resultFeedbackText->fetch()){     
?>
    <div><br/>Имя :
<? echo $rowFeedback['name'];?>
    <br/>
<?  echo $rowFeedbackText['text']; ?>
<hr/>
<? } ?>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>